# Bud collector

Ce dépôt rassemble les différents fichiers de configurations et les scripts pour déployer un service budcollector.

## Déploiement

On peut installer tous les fichiers de ce dépôt avec le Makefile :

    make install

Les détails du déploiement complet sont dans le wiki https://wiki.osug.fr/!isterre-geodata/resif/systemes/services/collecte_tqr_nœudb
