#!/bin/bash

# Ce script observe des fichiers de log et enregistre les chemins indiqués par un message de log dans la base de donnée d'inventaire

# In stdin, the message to scan
# Return codes:
#    0 : everything is fine, file registered
#    1 : file does not exist or not readable
#    2 : regex mismatch on filename
#    3 : error inserting data in database


# Selon la valeur de RUNMODE, on se branche sur différents environnements
# Les valeurs possibles sont production, preprod, test
echo "Starting with RUNMODE=$RUNMODE"
case $RUNMODE in
  "production")
    PGDATABASE="resifInv-Prod"
    PGUSER="budregister"
    PGHOST="resif-pgprod.u-ga.fr"
    PGPORT=5432
  ;;
  *)
    PGDATABASE="resifInv-Preprod"
    PGUSER="budregister"
    PGHOST="resif-pgpreprod.u-ga.fr"
    PGPORT=5432
  ;;
esac

export PGDATABASE PGUSER PGHOST PGPORT
#           network         station      location     channel           year        julian day
regex=".*\/([A-Z0-9]{1,2})\.([A-Z0-9]+)\.([A-Z0-9]{0,2})\.([A-Z0-9]{3})\.D\.([0-9]{4})\.([0-9]{3})$"

function no_validated_data_available(){
    # Test if validated data is distributed for the same day as the bud data we got
    # Returns 1 if data is already known by availability webservice
    # Else returns 0
    # $1 network
    # $2 station
    # $3 location
    # $4 channel
    # $5 year
    # $6 doy
    #
    startday=$(date -d "${5}-01-01 + $6 days - 1 day" +%Y-%m-%d)
    availability_params="network=$1&station=$2&location=${3:--}&channel=$4&start=${startday}T00:00:00&end=${startday}T23:59:59&quality=M"
    echo "Querying availability with $availability_params"
    if wget -qO- "https://ws.resif.fr/fdsnws/availability/1/query?nodata=404&$availability_params"; then
        echo "Validated data exists"
        return 1
    else
        echo "No validated data"
        return 0
    fi
}


# New data stream file created: /tmp/2020/FR/ILLK/HHE.D/FR.ILLK.00.HHE.D.2020.073
# Get the filename and check if exists, if not, it might be a relative path, try to get it from the bud mountpoint
while IFS='$\n' read -r line; do
    file=$(echo $line | awk 'NF{ print $NF }')
    limit=0; until [ $limit -gt 3 ] || [ -r "$file" ] ; do
        echo "File not yet readable"
        sleep 2
        limit=$(( limit+1 ))
    done
    if [[ $limit -gt 3 ]]; then
        echo "ERROR: $file not readable"
        exit 1
    fi

    if [[ $file =~ $regex ]]; then
        network=${BASH_REMATCH[1]}
        station=${BASH_REMATCH[2]}
        location=${BASH_REMATCH[3]:---}
        channel=${BASH_REMATCH[4]}
        year=${BASH_REMATCH[5]}
        dayofyear=${BASH_REMATCH[6]}
        name=$(basename $file)
        quality=$(msi -p $file | grep "^${network}_" | awk '{print $3; exit}')
        datestamp="${year}.${dayofyear}"
        # echo "DEBUG: $name quality $quality"
    else
        echo "ERROR: regex mismatch on $file"
        continue
    fi
    if no_validated_data_available $network $station $location $channel $year $dayofyear; then
        echo "$(date '+%Y-%m-%d %H:%M:%S') Registering $file with quality $quality, net: $network, sta: $station, loc: $location, cha: $channel, day: $datestamp"
        if ! psql -qtA -c " INSERT INTO  rbud (rbud_id, network, station, location ,channel,  starttime, endtime, source_file, quality, block_size, year, network_id, station_id, channel_id, availability )
        select nextval('rbud_id_seq'), '$network','$station', '$location', '$channel',
        TO_TIMESTAMP('$datestamp', 'YYYY.DDD'),
        TO_TIMESTAMP('$datestamp', 'YYYY.DDD') + CAST ('24h' AS INTERVAL),
        '$name', '$quality','512', '$year',
        set_network_id('$network', '$datestamp'),
        set_station_id_in_rbud('$network', '$station',
                            TO_TIMESTAMP('$datestamp', 'YYYY.DDD')::timestamp without time zone,
                            (TO_TIMESTAMP('$datestamp', 'YYYY.DDD') + CAST('24h' AS INTERVAL))::timestamp without time zone),
        set_channel_id('$network', '$station', '$location', '$channel',
                    TO_TIMESTAMP('$datestamp', 'YYYY.DDD')::timestamp without time zone,
                    (TO_TIMESTAMP('$datestamp', 'YYYY.DDD') + CAST('24h' AS INTERVAL))::timestamp without time zone),
        set_data_availability('$network','$year')
        WHERE
        NOT EXISTS (SELECT  b.source_file FROM rbud b WHERE source_file = '$name');"
        then
            echo "ERROR: psql insert $name in table rbud returned $?"
            continue
        fi
    else
        echo "Not registering $file because validated data exists for this day."
    fi
done

