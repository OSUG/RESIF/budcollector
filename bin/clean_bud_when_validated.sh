#!/usr/bin/env sh

# Ce script nettoie l'espace bud de toutes les données qui existent déjà dans l'espace des données validées.
#
# 1. Récupère dand l'inventaire la liste des fichiers concernés
# 2. Pour chaque ficher:
#    - si le vault n'en a pas une copie, mettre dans le vault
#    - supprimer le fichier de bud
#    - supprimer de la table rbud
# 3. Ensuite, relancer un budindexer si possible. Sinon, afficher un message d'instructions
#

# Selon la valeur de RUNMODE, on se branche sur différents environnements
# Les valeurs possibles sont production, preprod, test
echo "Starting with RUNMODE=$RUNMODE"
case $RUNMODE in
  "production")
    PGDATABASE="resifInv-Prod"
    PGUSER="resifinvprod"
    PGHOST="resif-pgprod.u-ga.fr"
    PGPORT=5432
  ;;
  *)
    PGDATABASE="resifInv-Preprod"
    PGUSER="resifinvdev"
    PGHOST="resif-pgpreprod.u-ga.fr"
    PGPORT=5432
  ;;
esac

export PGDATABASE PGUSER PGHOST PGPORT
#           network         station      location     channel           year        julian day
regex="^([A-Z0-9]{1,2})\.([A-Z0-9]+)\.([0-9]{0,2})\.([A-Z0-9]{3})\.D\.([0-9]{4})\.([0-9]{3})$"

vault_dir="/mnt/nfs/summer/vault"
bud_dir="/mnt/nfs/summer/bud_data"

files=$(psql -qtAc "select rbud.source_file from rbud join rall on rbud.source_file=rall.source_file;")

while IFS='$\n' read -r file; do
    if [[ $file =~ $regex ]]; then
        network=${BASH_REMATCH[1]}
        station=${BASH_REMATCH[2]}
        location=${BASH_REMATCH[3]}
        channel=${BASH_REMATCH[4]}
        year=${BASH_REMATCH[5]}
        dayofyear=${BASH_REMATCH[6]}
        datestamp="${year}.${dayofyear}"
    else
        echo "ERROR: regex mismatch on $file"
        continue
    fi
    vault_file="$vault_dir/$year/$network/$station/${channel}.D/$file.gz"
    bud_file="$bud_dir/$year/$network/$station/${channel}.D/$file"
    if [[ -r "$bud_file" ]] ; then
        # Si vault ne contient pas une copie du fichier, alors on l'ajoute
        if [[ -r "$vault_file" ]]; then
            mkdir -p $(dirname $vault_file)
            gzip -c "$bud_file" > "$vault_file"
        fi
        # On supprime le fichier bud
        rm "$bud_file"
    fi
    # Supprimer l'enregistrement dans la table rbud
    psql -qtAc "delete from rbud where rbud.source_file='$file'"
done <<< "$files"

echo "Nettoyage effectué. Il faut maintenant réindexer toutes les données brutes en utilisant budindexer.sh sur resif-vm33"
