#!/bin/bash
# Workaround to run the register_bud_file.sh script as sysop user with it's environment
su sysop -c "RUNMODE=production /home/sysop/.local/bin/register_bud_file.sh"
