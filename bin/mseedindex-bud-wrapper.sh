#!/usr/bin/env bash

# This script will index all new data in the BUD repository and write the indexation results in the inventroy database
# IMPORTANT prerequisite:
#   the script needs jq v1.7, and expect it to be installed in /usr/local/bin (hardcoded)
#   jq versions before that transform some big numbers in scientific notation which leads to unusable results in some cases.
#

rawdata=/mnt/nfs/summer/bud_data
PGHOST=resif-pgprod.u-ga.fr
PGUSER=budregister
PGDATABASE=resifInv-Prod
export rawdata
export PGHOST PGUSER PGDATABASE

run_interval=$(( 60*60*2 + 60 )) # 2 hours and one minute in seconds
loggercmd="logger -t bud-indexer -p info"

make_absolute_path(){
    filename=$1
    IFS='.' read -r -a filename_parts <<< $filename
    [ ${#filename_parts[@]} -lt 7 ] && echo "** $filename: wrong formatted filename ?"
    fullpath="${rawdata}/${filename_parts[5]}/${filename_parts[0]}/${filename_parts[1]}/${filename_parts[3]}.D/$filename"
    echo "$fullpath"
}

# Check if file has been modified since run_interval seconds
# Return 0 if not modified or not found, 1 if modified
check_file_modified() {
    file=$1
    last_changed=$(stat -c %Y $file)
    if [[ $? -eq 0 ]]; then
        if [[ $(( $(date +%s) - $last_changed )) -lt $run_interval ]]; then
            echo 1
            return
        fi
    fi
    echo 0
}

select_bud_files_to_index() {
    psql -qtAc "select source_file from rbud where created_at >= now() - '24 hours'::interval"
}

run_index() {
    source_file=$1
    $loggercmd "Running mseedindex on $source_file"
    mseedfile=$(make_absolute_path $source_file)
    if [[ $(check_file_modified $mseedfile) -eq 1 ]]; then
        msidx_json=$(mseedindex -json - $mseedfile)
        while read -r j; do
            timeidx=$(/usr/local/bin/jq -rc '.ts_time_byteoffset // []' <<< "$j")
            if [[ "$timeidx" != "" ]]; then
                spans=$(/usr/local/bin/jq -rc  '.ts_timespans // []' <<< "$j" )
                psql -q <<< "UPDATE rbud SET timeindex = '$timeidx', timespans = '$spans'  WHERE source_file='$source_file';"
            else
                $loggercmd "Unable to index $mseedfile"
            fi
        done <<< $(jq -rc ".\"${mseedfile}\".content[]" <<< $msidx_json)
    fi
}

# Export functions if to be used with GNU Parallel
# export -f run_index make_absolute_path check_file_modified

$loggercmd "Starting bud data indexation"
files=$(select_bud_files_to_index)
for f in $files; do
    # index files sequentially in order to not get too much IOs from NFS
    run_index $f
done
$loggercmd "End bud data indexation"
