install:
	install -m 755 bin/register_bud_file_wrapper.sh /usr/local/bin/register_bud_file_wrapper.sh
	install -d /etc/rsyslog.d
	install -m 644 etc/rsyslog.d/budcollector.conf /etc/rsyslog.d/budcollector.conf
	install -m 644 etc/supervisord.conf /etc/supervisord.conf
	install -m 644 etc/systemd/system/supervisor.service /etc/systemd/system/supervisor.service
	install -m 644 etc/logrotate.d/budcollector /etc/logrotate.d/budcollector
uninstall:
	rm /usr/local/bin/register_bud_file_wrapper.sh
	rm /etc/rsyslog.d/budcollector.conf
	rm /etc/supervisord.conf
	rm /etc/systemd/system/supervisor.service
	rm /etc/logrotate.d/budcollector
